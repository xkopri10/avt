package com.example.martin.myapplication;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.sql.Time;
import java.util.ArrayList;

/**
 * Created by Martin on 04.05.2017.
 */

public class DbHelperTime extends SQLiteOpenHelper {

    private  static final String DB_NAME="Database2";            //nastaveni jmena
    private static final int DB_VER = 1;                        //nastaveni verze me DB - po kazdem updatu by s emelo zvednout cislo (pro me)
    public static final String DB_TABLE = "Activity";               //jmeno cele tabulky
    public static final String DB_COLUM = "todo";
    public static final String DB_COLUM2 = "todo";

    public static final String ID = "id";
    public static final String DB_TYPE = "type";
    public static final String DB_TIME = "time";

    public DbHelperTime(Context context) {
        super(context, DB_NAME, null, DB_VER);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL("CREATE TABLE " + DB_TABLE + " (" +
                ID          + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                DB_TYPE       + " TEXT NOT NULL, " +
                DB_TIME       + " TEXT NOT NULL, " +
                 ")");
        /*
        String query = String.format("CREATE TABLE %s (ID INTEGER PRIMARY KEY AUTOINCREMENT, %s TEXT NOT NULL, );",DB_TABLE,DB_COLUM);
        db.execSQL(query);
        */
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + DB_TABLE); //toto je pre nas vyvojarska varianta, takto to nasadit na Googel je zle
        onCreate(db);
    }

    //vlozeni noveho zaznamu do tabulky
    public void insertNewActivity(String type, String time){
        SQLiteDatabase db = this.getWritableDatabase(); //udelma si zapisovatelnou DB
        ContentValues values = new ContentValues();     //vytvorim si promenou kterou tam nasledne ulozim - do ni ulozim nejake data
        values.put(DB_COLUM,type);                      //vlozeni hodnoty do zaznamu tabulky
        values.put(DB_COLUM,time);
        db.insert(DB_TABLE,null,values);
        db.close();      //po kazde by se mela db uzavrit
    }

    //metoda na cteni a vsech zaznamu v databazi
    public ArrayList<String> getTaskList(){ //vytvorim si array list
        ArrayList<String> taskList = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase(); //ted chci mod na cteni - ne na zapisovani
                                                        //pod tim bylo jen db colum
        Cursor cursor = db.query(DB_TABLE,new  String[]{DB_COLUM},null,null,null,null,null); //z jake tabulky? DB_TABLE vytvorim nove pole kde si vytahnu jen jedne COLUMN a vytahnu jen SELECT - zadny order by, having apod...
            while (cursor.moveToNext()){    //cyklus na vytazeni zaznamu a presunuti na dalsi pozici
                int index = cursor.getColumnIndex(DB_COLUM); //db colum tu bylo
                taskList.add(cursor.getString(index));
            }

        cursor.close(); //musim uzavrit
        db.close();     //take musim uzavrit
        return taskList;    //no a nakonec me vrat cely seznam z ArrayListu
    }
}
