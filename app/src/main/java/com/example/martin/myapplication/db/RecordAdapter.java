package com.example.martin.myapplication.db;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import com.example.martin.myapplication.R;

import org.w3c.dom.Text;

import java.util.Date;

/**
 * Created by Martin on 24.05.2017.
 */

public class RecordAdapter extends CursorAdapter {

    public RecordAdapter(Context context) {
        super(context, openCursor(context),true);
    }


    private static Cursor openCursor(Context context){
        MainOpenHelper openHelper = new MainOpenHelper(context);
        SQLiteDatabase db = openHelper.getReadableDatabase();
        return db.query(Record.TABLE,
                null,   //columns
                null,   //selection
                null,   //selection args
                null,   //group by
                null,   //having
                Record.DATE + " DESC"   //orderBy
        );
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        LayoutInflater inflater = ((Activity)context).getLayoutInflater();
        View view = inflater.inflate(R.layout.row_statistics,null,true);

        ViewHolder holder = new ViewHolder();
        holder.type = (TextView) view.findViewById(R.id.typeView);
        holder.time = (TextView) view.findViewById(R.id.timeView);
        holder.date = (TextView) view.findViewById(R.id.dateView);

        view.setTag(holder);    //tady si ulozim holder
        return view;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        Record record = new Record(cursor);
        ViewHolder holder = (ViewHolder) view.getTag();

        holder.type.setText("  " + record.getType());
        holder.time.setText(String.valueOf(record.getTime()));
        holder.date.setText(String.valueOf(record.getDate()));
    }

    private static class ViewHolder {
        public TextView type;
        public TextView time;
        public TextView date;

    }
}
