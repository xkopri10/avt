package com.example.martin.myapplication.db;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.provider.BaseColumns;
import android.provider.ContactsContract;

import java.util.Date;

/**
 * Created by Martin on 24.05.2017.
 */

public class Record {

    static final String TABLE = "note";
    static final String ID = BaseColumns._ID;
    static final String TYPE = "type";
    static final String TIME = "time";
    static final String DATE = "date";

    protected ContentValues values;

    public Record(){
        this.values = new ContentValues();
    }

    public Record(Cursor cursor){
        values = new ContentValues();
        DatabaseUtils.cursorRowToContentValues(cursor,values);
    }

    public Long getId(){
        return values.getAsLong(ID);
    }

    public String getType(){
        return values.getAsString(TYPE);
    }

    public void setType(String type){
        values.put(TYPE,type);
    }

    public Integer getTime(){
        return values.getAsInteger(TIME);
    }

    public void setTime(Integer time){
        values.put(TIME,time);
    }
/*
    public Date getDate() {
        return values.get(DATE)!=null ? new Date(values.getAsLong(DATE)) : null;
    }

    public void setDate(Date date){
        values.put(DATE, date.getTime());
    }
*/
    public String getDate(){
    return values.getAsString(DATE);
}

    public void setDate(String date){
        values.put(DATE,date);
    }

}
