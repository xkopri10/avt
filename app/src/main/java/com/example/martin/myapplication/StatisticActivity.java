package com.example.martin.myapplication;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.martin.myapplication.db.MainRepository;
import com.example.martin.myapplication.db.Record;
import com.example.martin.myapplication.db.RecordAdapter;

public class StatisticActivity extends ListActivity{

    private MainRepository repository;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_statistic);
        setListAdapter(new RecordAdapter(this));
        repository = new MainRepository(this);


    }
    @Override
    protected void onResume() {
        super.onResume();
        setListAdapter(new RecordAdapter(this));
    }


}
