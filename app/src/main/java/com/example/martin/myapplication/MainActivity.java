package com.example.martin.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

public class MainActivity extends Activity {

    private ImageButton timeManagerButton;
    private ImageButton taskManagerButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

       // timeManagerButton = (ImageButton) findViewById(R.id.timeManager);
        //taskManagerButton = (ImageButton) findViewById(R.id.taskManager);
    }

    public void timeManagerClick(View view){
        Intent intent = new Intent(this,TimeManagerActivity.class);
        startActivity(intent);
    }

    public void taskManagerClick(View view){
        Intent intent = new Intent(this,TaskManagerActivity.class);
        startActivity(intent);
    }
}
