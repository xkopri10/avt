package com.example.martin.myapplication;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class TaskManagerActivity extends Activity {

    DbHelper dbHelper;
    ArrayAdapter<String> mAdapter;
    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_manager);

        dbHelper = new DbHelper(this);

        listView = (ListView) findViewById(R.id.listTaskView);
        loadTaskList();



    }

    private void loadTaskList() {
        ArrayList<String> taskList = dbHelper.getTaskList();
        if(mAdapter==null){
            mAdapter = new ArrayAdapter<String>(this,R.layout.row,R.id.task_title,taskList);
            listView.setAdapter(mAdapter);
        }else{
            mAdapter.clear();
            mAdapter.addAll(taskList);
            mAdapter.notifyDataSetChanged();
        }
    }

    public void clickOnPlus(View view){
        final EditText taskEditText = new EditText(this);   //vytvorim si edit text kterej niž priradim v tom dialog oknu
        AlertDialog dialog = new AlertDialog.Builder(this)  //vyskakovaci okno
                .setTitle("Přidej nový úkol")               //titulek
                .setView(taskEditText)                      //zde priradim ten muj vytvorenej edit text
                .setPositiveButton("Přidej", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {        //vygenerovane - na pridani zaznamu
                        String task = String.valueOf(taskEditText.getText());
                        dbHelper.insertNewTask(task);
                        loadTaskList();
                    }
                })
                .setNegativeButton("Zrušit",null)   //tlacitko an zrseni
                .create();                          // no a ted to vse vytvor
        dialog.show();  //ukaz mi dialog

    }

    //metoda na odstraneni (na tlacitko delete) ukolu ze seznamu
    public void deleteTask(View view){
        View parent = (View)view.getParent();
        TextView taskTextView = (TextView) parent.findViewById(R.id.task_title);
        String task = String.valueOf(taskTextView.getText());
        dbHelper.deleteTask(task);
        loadTaskList();
    }


}
