package com.example.martin.myapplication.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Martin on 24.05.2017.
 */

public class MainOpenHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "main.db";
    private static final int DATABASE_VERSION = 5;

    public MainOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + Record.TABLE + " (" +
                Record.ID          + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                Record.TYPE        + " TEXT NOT NULL, " +
                Record.TIME        + " INTEGER, " +
                Record.DATE        + " TEXT " +
                ")"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + Record.TABLE);
        onCreate(db);
    }
}
