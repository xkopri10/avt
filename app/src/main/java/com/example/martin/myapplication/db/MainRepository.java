package com.example.martin.myapplication.db;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Martin on 24.05.2017.
 */

public class MainRepository {
    private MainOpenHelper mainOpenHelper;

    public MainRepository(Context context){
        mainOpenHelper = new MainOpenHelper(context);
    }

    public long insert(Record record){
        SQLiteDatabase db = mainOpenHelper.getWritableDatabase();
        try {
            return db.insert(Record.TABLE,null,record.values);
        }finally {
            db.close();
        }
    }

    public long delete(long id){
        SQLiteDatabase db = mainOpenHelper.getWritableDatabase();
        try {
            return db.delete(Record.TABLE,"_id =" + id, null);
        } finally {
            db.close();
        }

    }

    public Record findViewById(long id){
        SQLiteDatabase db = mainOpenHelper.getReadableDatabase();
        try {
            List<Record> result = new LinkedList<>();
            Cursor cursor = db.query(Record.TABLE,
                    null,   //columns
                    Record.ID + " = " + id,   //selection
                    null,   //selection args
                    null,   //group by
                    null,   //having
                    null    //orderBy
            );
            try {
                if (cursor.moveToNext())
                    return new Record(cursor);
                throw new IllegalArgumentException("ID neni validni");
            }finally {
                cursor.close();
            }
        }finally {
            db.close();
        }
    }

}
