package com.example.martin.myapplication;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

/**
 * Created by Martin on 29.04.2017.
 */

public class DbHelper extends SQLiteOpenHelper {

    private  static final String DB_NAME="Database";            //nastaveni jmena
    private static final int DB_VER = 1;                        //nastaveni verze me DB - po kazdem updatu by s emelo zvednout cislo (pro me)
    public static final String DB_TABLE = "Task";               //jmeno cele tabulky
    public static final String DB_COLUM = "TaskName";           //jmeno zaznamu

    public DbHelper(Context context) {
        super(context, DB_NAME, null, DB_VER);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String query = String.format("CREATE TABLE %s (ID INTEGER PRIMARY KEY AUTOINCREMENT, %s TEXT NOT NULL);",DB_TABLE,DB_COLUM);
        db.execSQL(query);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String query = String.format("DELETE TABLE IF EXISTS %s",DB_TABLE);
        db.execSQL(query);
        onCreate(db);
    }

    //vlozeni noveho zaznamu do tabulky
    public void insertNewTask(String task){
        SQLiteDatabase db = this.getWritableDatabase(); //udelma si zapisovatelnou DB
        ContentValues values = new ContentValues();     //vytvorim si promenou kterou tam nasledne ulozim - do ni ulozim nejake data
        values.put(DB_COLUM,task);                      //vlozeni hodnoty do zaznamu tabulky
        db.insertWithOnConflict(DB_TABLE,null,values,SQLiteDatabase.CONFLICT_REPLACE);
        db.close();      //po kazde by se mela db uzavrit
    }

    //metoda na odstraneni ukolu z DB
    public void deleteTask(String task){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(DB_TABLE,DB_COLUM + " = ?",new String[]{task});       //anotace z android stranek
        db.close();     //opet utavreni DB !!
    }

    //metoda na cteni a vsech zaznamu v databazi
    public ArrayList<String> getTaskList(){ //vytvorim si array list
        ArrayList<String> taskList = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase(); //ted chci mod na cteni - ne na zapisovani
        Cursor cursor = db.query(DB_TABLE,new  String[]{DB_COLUM},null,null,null,null,null); //z jake tabulky? DB_TABLE vytvorim nove pole kde si vytahnu jen jedne COLUMN a vytahnu jen SELECT - zadny order by, having apod...
        while (cursor.moveToNext()){    //cyklus na vytazeni zaznamu a presunuti na dalsi pozici
            int index = cursor.getColumnIndex(DB_COLUM);
            taskList.add(cursor.getString(index));
        }
        cursor.close(); //musim uzavrit
        db.close();     //take musim uzavrit
        return taskList;    //no a nakonec me vrat cely seznam z ArrayListu
    }
}
